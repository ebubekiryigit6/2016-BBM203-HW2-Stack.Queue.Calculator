/**
 *
 * Author:      Ebubekir Yiğit
 *
 * ID:          21328629
 *
 * File:        exp2.c
 *
 * Purpose:     Stack Based Integer and Hex Calculator
 *
 *              Implemented Queue Struct
 *              Implemented Stack Struct
 *
 *              This program read a txt file with format --->
 *              calculate integer "1+-2/5"
 *              calculate hex "A-B*3"
 *
 *
 * Compile:     gcc -o exp2 -g -Wall exp2.c
 *
 * Run:         ./exp2 <input-file> <output-file>
 *
 *              files' extension is .txt
 *
 *
 * Input:       none
 *
 * Output:      Commands parsed, calculated with stack and results added in Queue.
 *
 *              Output is the Queue's elements.
 *
 *
 * Notes:
 *
 *              Program can gives us segmentation fault or wrong output.
 *              Algorithm is not implemented.
 *              We had many exams.
 *              Sorry.
 *
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct {
    char** array;
    int top;
    int size;
}Stack;

typedef struct {
    char** array;
    int front;
    int rear;
    int size;
}Queue;


void readFile(FILE *output, char *inputFileName, Queue *mainQueue);
void lineParser(FILE *output, char *line,Queue *mainQueue);
Queue parseExpressions(char* expression);
int isOperatorParantheses(char p);
int isHexorInteger(char p) ;
int isExpressionCorrect(char* expression);
void initializeQueue(Queue *queue, int initialSize) ;
void printQueue(Queue *queue);
void printCommand(FILE *output, Queue *mainQueue);
int isLeftParantheses(char p) ;
int isRightParantheses(char p) ;
int isParanthesesBalanced(char* expression);
int isThereParantheses(char* expression);
int isOperator(char p) ;
int isThereThreeOperators(char* expression);
int isOperatorError(char* expression);
int isInHex(char* expression);
void calculateExpression(char* operation,Queue *mainQueue, int type);
int endsWithorStartsWith(char* expression);
void calculator(Queue *mainQueue,char* operation, int type);
int expressionPriority(char p);
int isDigit(char p) ;
char* performOperator(char *value1, char* value2, char p) ;


#define CALCULATE "calculate"
#define PRINT "print"
#define PRINT_NL "print\n"
#define PRINT_DEV "print\r\n"
#define INTEGER "integer"
#define HEX "hex"
#define NEWLINE "\r\n"
#define BUFFER_SIZE 1000000
#define ERROR "error\0"


int main(int argc, char* argv[]) {

    Queue mainQueue;
    initializeQueue(&mainQueue,1);

    FILE *output;
    output = fopen(argv[2],"w");

    readFile(output,argv[1],&mainQueue);

    fclose(output);

    printQueue(&mainQueue);

    return 0;
}


/**
 * This is a stack initializer.
 * Programmer gives a initial size, while adding elements array size increased.
 *
 * @param stack --Stack struct
 * @param initialSize --can be 1.
 */
void initializeStack(Stack *stack, int initialSize) {
    stack->array = (char **)malloc(initialSize * sizeof(char*));
    stack->top = -1;
    stack->size = initialSize;
}

/**
 * Adds an element in stack.
 * Size will increase itself dont worry.
 *
 * @param stack
 * @param element
 */
void pushStack(Stack *stack, char* element){
    if (stack->top == stack->size - 1){
        stack->size *= 2;
        stack->array = (char**)realloc(stack->array, stack->size * sizeof(char*));
    }
    stack->top++;
    stack->array[stack->top] = element;
}

/**
 * Pops stack's top element.
 *
 * @param stack
 * @return
 */
char* popStack(Stack *stack){
    char* topChar;
    if (stack->top == -1){
        return "-1\0";
    }
    else {
        topChar = stack->array[stack->top];
        stack->top--;
        return topChar;
    }
}

/**
 * returns stack's element count
 *
 * @param stack
 * @return
 */
int stackElementCount(Stack *stack){
    if (stack->top == -1){
        return 0;
    }
    else {
        return stack->top + 1;
    }
}

/**
 * Prints out stack elements with format (Stack Element [index] = element)
 *
 * @param stack
 */
void printStack(Stack *stack){
    printf("Begining stack\n");
    if (stack->top == -1){
        printf("Stack is Empty !\n");
    }
    else {
        int i;
        for (i = 0; i <= stack->top; i++){
            printf("Stack Element [%d] = %s\n",i,stack->array[i]);
        }
    }
    printf("End of stack\n");
}

/**
 * returns true if stack is empty.
 *
 * @param stack
 * @return
 */
int isEmptyStack(Stack *stack){
    int temp = 0;
    if (stack->top == -1){
        temp = 1;
    }
    return temp;
}

/**
 * returns stack's top element, no pop
 *
 * @param stack
 * @return
 */
char* topStack(Stack *stack){
    if (stack->top == -1){
        printf("Stack is Empty !\n");
        return "-1\0";
    }
    else {
        return stack->array[stack->top];
    }
}

/**
 * Queue initializer.
 * Programmer gives a initial size, while adding elements array size increased.
 *
 * @param queue
 * @param initialSize
 */
void initializeQueue(Queue *queue, int initialSize) {
    queue->array = (char**)malloc(initialSize * sizeof(char*));
    queue->front = -1;
    queue->rear = -1;
    queue->size = initialSize;
}

/**
 * Adds queue array one element.
 * Array size will increase itself.
 *
 * @param queue
 * @param element
 */
void enqueue(Queue *queue, char* element){
    if (queue->rear == queue->size -1){
        queue->size *= 2;
        queue->array = (char**)realloc(queue->array, queue->size * sizeof(char*));
    }
    if (queue->front == -1){
        queue->front = 0;
    }
    queue->rear++;
    queue->array[queue->rear] = element;
}

/**
 * Deletes an item from Queue's front
 * and returns it.
 *
 * @param queue
 * @return
 */
char* dequeue(Queue *queue){
    char* frontQueue;
    if (queue->front == -1 || queue->front > queue->rear){
        printf("Empty Queue !\n");
        return NULL;
    }
    else {
        frontQueue = queue->array[queue->front];
        queue->front++;
        return frontQueue;
    }
}

/**
 * Returns front item of Queue.
 * But not delete.
 *
 * @param queue
 * @return
 */
char* front(Queue *queue){
    if (queue->front == -1 || queue->front > queue->rear){
        printf("Empty Queue !\n");
        return NULL;
    }
    else {
        return queue->array[queue->front];
    }
}

/**
 * returns true if queue is empty. else false
 *
 * @param queue
 * @return
 */
int isEmptyQueue(Queue *queue){
    if (queue->front == -1 || queue->front > queue->rear){
        return 1;
    }
    else {
        return 0;
    }
}

/**
 * Prints out queue elements with format (Queue Element [index] = element)
 * @param queue
 */
void printQueue(Queue *queue){
    if (queue->front == -1 || queue->front > queue->rear){

    }
    else {
        int i;
        for (i = queue->front; i <= queue->rear; i++){
            printf("Queue Element [%d] = %s\n",i,queue->array[i]);
        }
    }
}

/**
 * reads input file
 *
 * reads a line and sends lineParser
 *
 * @param output
 * @param inputFileName
 * @param mainQueue
 */
void readFile(FILE *output, char *inputFileName, Queue *mainQueue){

    FILE *file = fopen (inputFileName, "r");
    if ( file != NULL ) {
        char line [BUFFER_SIZE];
        while (fgets (line, sizeof line, file) != NULL )
        {
            lineParser(output,line,mainQueue);
        }
        fclose (file);
    }
    else {
        perror (inputFileName);
    }
}

/**
 * Parse the line and sends corresponding function
 * If line is "print"  sends print function
 * If line is "hex" and "integer" calculateExpression function
 *
 * @param output
 * @param singleLine
 * @param mainQueue
 */
void lineParser(FILE *output, char* singleLine,Queue *mainQueue){

    char* token;
    char temp[BUFFER_SIZE];
    strcpy(temp,singleLine);

    token = strtok(temp," ");

    if (strcmp(token,PRINT) == 0 || strcmp(token,PRINT_NL) == 0 || strcmp(token,PRINT_DEV) == 0){
        printCommand(output,mainQueue);
    }
    else if (strcmp(token,CALCULATE) == 0){
        token = strtok(NULL," ");
        if (strcmp(token,INTEGER) == 0){
            token = strtok(NULL," ");
            calculateExpression(token,mainQueue,1);
        }
        else if (strcmp(token,HEX) == 0){
            token = strtok(NULL," ");
            calculateExpression(token,mainQueue,0);
        }
    }
}

/**
 * The line is print command.
 * Write element of Queue output file and clear out queue
 *
 * @param output
 * @param mainQueue
 */
void printCommand(FILE *output, Queue *mainQueue){
    if (isEmptyQueue(mainQueue)){
        fprintf(output,"");
    } else {
        int i;
        for (i = mainQueue->front; i <= mainQueue->rear; i++){
            fprintf(output,"%s\n",mainQueue->array[i]);
        }

        while (!isEmptyQueue(mainQueue)){
            dequeue(mainQueue);
        }

    }
}

/**
 * Looks expression Is there any error.
 * If there is no error sends expression calculator.
 *
 * @param operation
 * @param mainQueue
 * @param type
 */
void calculateExpression(char* operation,Queue *mainQueue, int type){
    char *token = "";
    if (operation != NULL){
        token = strtok(operation,NEWLINE);
    }

    if (!isExpressionCorrect(token)){
        enqueue(mainQueue,ERROR);
    }
    else if (isThereParantheses(operation) && !isParanthesesBalanced(operation)){
        enqueue(mainQueue,ERROR);
    }
    else if (isThereThreeOperators(operation)){
        enqueue(mainQueue,ERROR);
    }
    else if (isOperatorError(operation)){
        enqueue(mainQueue,ERROR);
    }
    else if (type == 1 && isInHex(operation)) {
        enqueue(mainQueue,ERROR);
    }
    else if (endsWithorStartsWith(operation)){
        enqueue(mainQueue,ERROR);
    }
    else {

        calculator(mainQueue,operation,type);

    }
}

/**
 * calculator calculates expression and adds Queue its result.
 *
 * @param mainQueue
 * @param operation
 * @param type
 */
void calculator(Queue *mainQueue,char* operation, int type){

    Stack operatorStack;
    Stack operandStack;

    initializeStack(&operatorStack, 1);
    initializeStack(&operandStack, 1);

    Queue parsedExpression = parseExpressions(operation);


    if (parsedExpression.front == -1 || parsedExpression.front > parsedExpression.rear) {
        printf("PARSE ERROR: Queue is Empty !\n");
    } else {
        int i;
        for (i = parsedExpression.front; i <= parsedExpression.rear; i++) {
            if (isLeftParantheses(parsedExpression.array[i][0])) {
                pushStack(&operatorStack,parsedExpression.array[i]);
            }
            else if (isHexorInteger(parsedExpression.array[i][0])){
                pushStack(&operandStack,parsedExpression.array[i]);
            }
            else if (isOperator(parsedExpression.array[i][0])) {
                if (isEmptyStack(&operatorStack)){
                    pushStack(&operatorStack,parsedExpression.array[i]);
                }
                else if (isOperator(topStack(&operatorStack)[0])){
                    if (expressionPriority(topStack(&operatorStack)[0]) >= expressionPriority(parsedExpression.array[i][0])){
                        char* firstItem = popStack(&operandStack);
                        char* secondItem = popStack(&operandStack);
                        pushStack(&operandStack,performOperator(firstItem,secondItem,popStack(&operatorStack)[0]));
                        pushStack(&operatorStack,parsedExpression.array[i]);
                    }
                    else {
                        pushStack(&operatorStack,parsedExpression.array[i]);
                    }
                }
                else {
                    pushStack(&operatorStack,parsedExpression.array[i]);
                }
            }
            else if (isRightParantheses(parsedExpression.array[i][0])) {
                if (isLeftParantheses(topStack(&operatorStack)[0])){
                    popStack(&operatorStack);
                }
                else if (isOperator(topStack(&operatorStack)[0])){
                    while (!isLeftParantheses(topStack(&operatorStack)[0])){
                        char* firstItem = popStack(&operandStack);
                        char* secondItem = popStack(&operandStack);
                        pushStack(&operandStack,performOperator(firstItem,secondItem,popStack(&operatorStack)[0]));
                    }
                    popStack(&operatorStack);
                }
            }
        }
        while (!isEmptyStack(&operatorStack) || stackElementCount(&operandStack) != 1){
            char* firstItem = popStack(&operandStack);
            char* secondItem = popStack(&operandStack);
            pushStack(&operandStack,performOperator(firstItem,secondItem,popStack(&operatorStack)[0]));
        }
    }
    if (type == 1){
        char* temp = malloc(30 * sizeof(char));
        temp[0] = '\0';
        strcat(temp,"integer \0");
        strcat(temp,topStack(&operandStack));
        enqueue(mainQueue,temp);

    } else if (type == 0){
        char* temp = malloc(30 * sizeof(char));
        temp[0] = '\0';
        strcat(temp,"hex \0");
        strcat(temp,topStack(&operandStack));
        enqueue(mainQueue,temp);
    }

}

/**
 * "+"  "-"  "*"  "/"
 * returns char result.
 *
 * @param value1
 * @param value2
 * @param p
 * @return
 */
char* performOperator(char *value1, char* value2, char p) {
    int a = atoi(value1);
    int b = atoi(value2);

    char* result = malloc(30 * sizeof(char));
    result[0] = '\0';

    switch (p) {
        case '+':
            snprintf(result,30,"%d",a+b);
            break;
        case '-':
            snprintf(result,30,"%d",b-a);
            break;
        case '*':
            snprintf(result,30,"%d",a*b);
            break;
        case '/':
            snprintf(result,30,"%d",b/a);
            break;
        default:
            strcat(result,"202303404505");
    }
    return result;
}

/**
 * Is expression starts with operator
 * Is expression ends with operator
 *
 * @param expression
 * @return
 */
int endsWithorStartsWith(char* expression){
    int len = strlen(expression);
    if (len > 3){
        if (isOperator(expression[1]) && isOperator(expression[2])){
            return 1;
        }
    }
    if (len > 2) {
        if (isOperator(expression[len - 2])) {
            return 1;
        } else if (expression[1] == '*' || expression[1] == '/') {
            return 1;
        }
    }
    else {
        return 0;
    }
    return 0;
}

/**
 * Is there any operator error like "2**4" or "2-/3"
 *
 * @param expression
 * @return
 */
int isOperatorError(char* expression){
    if (strstr(expression,"//") || strstr(expression,"**") || strstr(expression,"/*") || strstr(expression,"*/")
        || strstr(expression,"-/") || strstr(expression,"-*") || strstr(expression,"+/") || strstr(expression,"+*")
        || strstr(expression," ") || strstr(expression,"/0") || strstr(expression,"()")){

        return 1;
    } else {
        return 0;
    }
}

/**
 * Is there three operator like "2---3"
 *
 * @param expression
 * @return
 */
int isThereThreeOperators(char* expression){
    int i;
    int len = strlen(expression);
    int temp = 0;
    if (len >= 3) {
        for (i = 0; i < len - 2; i++) {
            if (isOperator(expression[i]) && isOperator(expression[i + 1]) && isOperator(expression[i + 2])) {
                temp = 1;
                break;
            }
        }
    } else {
        temp = 0;
    }
    return temp;
}

/**
 * Is there any parantheses in expression
 *
 * @param expression
 * @return
 */
int isThereParantheses(char* expression){
    if (strchr(expression,'(') || strchr(expression,')')){
        return 1;
    } else{
        return 0;
    }
}

/**
 * Calculates parantheses is balanced in expression with STACK.
 *
 * @param expression
 * @return
 */
int isParanthesesBalanced(char* expression){

    Stack operatorStack;
    initializeStack(&operatorStack,1);

    char leftP[2] = {'(','\0'};

    int len = strlen(expression);
    int i;
    for (i = 0; i < len; i++){
        if (isLeftParantheses(expression[i])){
            char temp[2];
            temp[0] = expression[i];
            temp[1] = '\0';
            pushStack(&operatorStack,temp);
        }
        else if (isRightParantheses(expression[i])){
            if (!isEmptyStack(&operatorStack)){
                if (strcmp(topStack(&operatorStack),leftP) == 0){
                    popStack(&operatorStack);
                }
                else {
                    return 0;
                }
            }
            else {
                return 0;
            }
        }
    }
    return isEmptyStack(&operatorStack);
}

/**
 * Any other character except "()+-/*" and number and hexs
 *
 * @param expression
 * @return
 */
int isExpressionCorrect(char* expression){
    int temp = 0;
    int i,len = strlen(expression);
    for (i = 0; i < len; i++){
        if (!isOperatorParantheses(expression[i]) && !isHexorInteger(expression[i]) && expression[i] != '"'){
            temp = 0;
            break;
        } else {
            temp = 1;
        }
    }
    return temp;
}

/**
 * Is expression stores hex chars
 *
 * @param expression
 * @return
 */
int isInHex(char* expression){
    int i;
    int temp = 0;
    int len = strlen(expression);
    for (i = 0; i < len; i++){
        char p = expression[i];
        if (p == 'a' || p =='A' || p =='b' || p =='B' || p =='c' || p =='C'  || p == 'd' || p == 'D' || p == 'e' || p == 'E' || p == 'f'|| p == 'F'){
            temp = 1;
            break;
        }
    }
    return temp;
}

/**
 * char is digit?
 *
 * @param p
 * @return
 */
int isDigit(char p) {
    if (p >= '0' && p <= '9') {
        return 1;
    }
    else {
        return 0;
    }
}

/**
 * is char left parantheses.
 *
 * @param p
 * @return
 */
int isLeftParantheses(char p) {
    if (p == '('){
        return 1;
    }
    else {
        return 0;
    }
}

/**
 * is char right parantheses.
 *
 * @param p
 * @return
 */
int isRightParantheses(char p) {
    if (p == ')'){
        return 1;
    }
    else {
        return 0;
    }
}
/**
 * is char an operator.
 *
 * @param p
 * @return
 */
int isOperator(char p) {
    if (p == '+' || p == '-' || p == '*' || p == '/') {
        return 1;
    }
    else {
        return 0;
    }
}

/**
 * is char hex or integer.
 *
 * @param p
 * @return
 */
int isHexorInteger(char p) {
    if (isDigit(p) || p == 'a' || p =='A' || p =='b' || p =='B' || p =='c' || p =='C'  || p == 'd' || p == 'D' || p == 'e' || p == 'E' || p == 'f'|| p == 'F'){
        return 1;
    }
    else {
        return 0;
    }
}

/**
 * is char operator or parantheses.
 *
 * @param p
 * @return
 */
int isOperatorParantheses(char p){
    if (isRightParantheses(p) || isLeftParantheses(p) || isOperator(p)){
        return 1;
    } else {
        return 0;
    }
}

/**
 * "-" and "+" operators is 1
 * "*" and "/" operators is 2
 *
 * @param p
 * @return
 */
int expressionPriority(char p){
    switch (p){
        case '*':
            return 2;
        case '+':
            return 1;
        case '/':
            return 2;
        case '-':
            return 1;
        default:
            printf("islem onceligi fonksiyonuna yanlis degisken atandi!");
            return 0;
    }
}

/**
 * Parses and expression and adds it a Queue.
 *
 * @param expression
 * @return
 */
Queue parseExpressions(char* expression){
    Queue expressionQueue;
    initializeQueue(&expressionQueue,1);

    int lenExpression = strlen(expression);
    int i;
    for (i = 0; i < lenExpression; i++) {

        if (expression[i] != '"') {
            /* char is not number */
            if (isOperatorParantheses(expression[i]) == 1 && expression[i] != '"') {
                char *operator = malloc(2 * sizeof(char));
                operator[0] = expression[i];
                operator[1] = '\0';
                enqueue(&expressionQueue, operator);
                /*  char is hex or integer */
            } else if (isHexorInteger(expression[i]) && expression[i] != '"') {
                char *number = malloc(lenExpression * sizeof(char));
                int j = 0;
                while (isHexorInteger(expression[i]) == 1 && expression[i] != '"' && i < lenExpression) {
                    number[j] = expression[i];
                    i++;
                    j++;
                }
                number[j] = '\0';
                enqueue(&expressionQueue, number);
                i--;
            }
        }
    }

    return expressionQueue;
}